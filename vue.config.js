'use strict'
// path依赖
const path = require('path')
// 代码压缩
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
// gzip压缩
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const resolve = dir => path.resolve(__dirname, dir)
// 是否为生产环境
const isProduction = process.env.NODE_ENV === 'production'
const name = '后台管理系统' // 网页标题
const port = process.env.port || 80 // 端口

module.exports = {
	publicPath: isProduction ? './' : '/', // 基本路径
	outputDir: 'dist', // 输出文件目录
	assetsDir: 'static', // 相对于outputDir的静态资源(js、css、img、fonts)目录
	lintOnSave: false, // eslint-loader 是否在保存的时候检查
	runtimeCompiler: false, //是否使用包含运行时编译器的 Vue 构建版本
	productionSourceMap: false, // 生产环境是否生成 sourceMap 文件
	// 链式配置
	chainWebpack: config => {
		//修复 Lazy loading routes Error
		config.plugin('html').tap(args => {
			args[0].chunksSortMode = 'none';
			return args;
		});
		config.plugins.delete('preload') // TODO: need test
		config.plugins.delete('prefetch') // TODO: need test
		// 添加别名
		config.resolve.alias
			.set('@', resolve('src'))
			.set('components', resolve('src/components'))
			.set('views', resolve('src/views'))

		// set svg-sprite-loader
		config.module
			.rule('svg')
			.exclude.add(resolve('src/assets/icons'))
			.end()
		config.module
			.rule('icons')
			.test(/\.svg$/)
			.include.add(resolve('src/assets/icons'))
			.end()
			.use('svg-sprite-loader')
			.loader('svg-sprite-loader')
			.options({
				symbolId: 'icon-[name]'
			})
			.end()

		// set preserveWhitespace
		config.module
			.rule('vue')
			.use('vue-loader')
			.loader('vue-loader')
			.tap(options => {
				options.compilerOptions.preserveWhitespace = true
				return options
			})
			.end()

		config
			// https://webpack.js.org/configuration/devtool/#development
			.when(process.env.NODE_ENV === 'development',
				config => config.devtool('cheap-source-map')
			)

		config
			.when(process.env.NODE_ENV !== 'development',
				config => {
					config
						.plugin('ScriptExtHtmlWebpackPlugin')
						.after('html')
						.use('script-ext-html-webpack-plugin', [{
							// `runtime` must same as runtimeChunk name. default is `runtime`
							inline: /runtime\..*\.js$/
						}])
						.end()
					config
						.optimization.splitChunks({
							chunks: 'all',
							cacheGroups: {
								libs: {
									name: 'chunk-libs',
									test: /[\\/]node_modules[\\/]/,
									priority: 10,
									chunks: 'initial' // only package third parties that are initially dependent
								},
								elementUI: {
									name: 'chunk-elementUI', // split elementUI into a single package
									priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
									test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
								},
								commons: {
									name: 'chunk-commons',
									test: resolve('src/components'), // can customize your rules
									minChunks: 3, //  minimum common number
									priority: 5,
									reuseExistingChunk: true
								},
								styles: {
									name: 'styles',
									test: /\.(sa|sc|c)ss$/,
									chunks: 'all',
									enforce: true
								}
							}
						})
					config.optimization.runtimeChunk('single')
				}
			)
	},
	// 简单/基础配置，比如引入一个新插件
	configureWebpack: config => {
		config.name = name
		if (isProduction) {
			// 为生产环境修改配置...
			const plugins = []
			plugins.push(
				new UglifyJsPlugin({
					uglifyOptions: {
						compress: {
							warnings: false,
							drop_console: true,
							drop_debugger: true,
							pure_funcs: ['console.log'] //移除console
						},
						mangle: false,
						output: {
							beautify: true, //压缩注释
						}
					},
					sourceMap: false,
					parallel: true,
				})
			)
			plugins.push( // 使用gzip打包压缩css/js
				new CompressionWebpackPlugin({
					filename: '[path].gz[query]',
					algorithm: 'gzip',
					test: new RegExp('\\.(js|css)$'),
					threshold: 10240,
					minRatio: 0.8
				})
			)
			config.plugins = [...config.plugins, ...plugins]
		}
	},
	// css相关配置
	css: {
		extract: true, // 是否使用css分离插件 ExtractTextPlugin
		sourceMap: false, // 开启 CSS source maps?
		loaderOptions: {

		}, // css预设器配置项
		// modules: false // 启用 CSS modules for all css / pre-processor files.
	},
	parallel: require('os').cpus().length > 1,
	pwa: {},
	// webpack-dev-server 相关配置
	devServer: {
		open: true, //配置自动启动浏览器
		host: 'localhost', //IP
		port: port, //端口号
		https: false,
		hotOnly: false,
		proxy: { // 配置跨域，打包后无效
			'/api': {
				target: 'http://localhost:8080',
				//'https://www.fastmock.site/mock/807d0e80f1cdf78faa8e187716196eee/api',//后端接口地址
				changeOrigin: true, //是否允许跨越
				ws: true, //是否代理websockets
				pathRewrite: {
					'^/api': '', //重写,
				}
			}
		}, // 配置跨域处理
		disableHostCheck: true,
		before: app => {}
	},

	// 第三方插件配置
	pluginOptions: {}
}
