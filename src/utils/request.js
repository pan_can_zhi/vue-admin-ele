import axios from 'axios'
import store from '@/store'
import { Notification, MessageBox, Message } from 'element-ui'
import { getToken } from '@/utils/auth'

/****** 创建axios实例 ******/
const service = axios.create({
	baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
	withCredentials: true, // 开启跨域
	timeout: 5000, // 请求超时时间，毫秒
	headers: {
		'Content-Type': 'application/json;charset=utf-8'
	}
})

/****** request拦截器==>对请求参数做处理 ******/
service.interceptors.request.use(config => {
	if (getToken()) {
		config.headers['Authorization'] = 'Bearer ' + getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
	}
	return config
  },
  error => {return Promise.reject(error)}
)

/****** respone拦截器==>对响应做处理 ******/
service.interceptors.response.use(
  response => {
    const code = response.status
    if (code < 200 || code > 300) {
			MessageBox.alert(response.message)
      return Promise.reject('error')
    } else {
      return response.data
    }
  },
  error => {
    let code = 0
    try {
      code = error.response.data.status
    } catch (e) {
      if (error.toString().indexOf('Error: timeout') !== -1) {
				MessageBox.alert('网络请求超时')
        return Promise.reject(error)
      }
      if (error.toString().indexOf('Error: Network Error') !== -1) {
				MessageBox.alert('网络请求错误')
        return Promise.reject(error)
      }
    }
    if (code === 401) {
      MessageBox.confirm(
			 '登录状态已过期，您可以继续留在该页面，或者重新登录',
			 '系统提示',
			 {
				 confirmButtonText: '重新登录',
				 cancelButtonText: '取消',
				 type: 'warning'
			 }
       ).then(() => {
        store.dispatch('LogOut').then(() => {
          location.reload() // 为了重新实例化vue-router对象 避免bug
        })
      })
    } else if (code === 403) {
			MessageBox.alert('对不起，你没有访问权限！')
    } else {
      const errorMsg = error.response.data.message
      if (errorMsg) {
				MessageBox.alert(errorMsg)
      }
    }
    return Promise.reject(error)
  }
)
export default service