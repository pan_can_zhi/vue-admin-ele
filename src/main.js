import Vue from 'vue'
import 'normalize.css/normalize.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n
import './assets/styles/index.scss' // global css

// 权限指令
import permission from './directive/permission'
import './utils/directives'
import Pagination from "@/components/Pagination"
import App from './App'
import router from './router'
import './permission' // permission control
import store from './store'
import './assets/icons' // icon
import { getDicts } from "@/api/system/dict/data";
import { getConfigKey } from "@/api/system/config";
import { parseTime, resetForm, addDateRange, selectDictLabel, download, handleTree } from "@/utils";

Vue.use(permission)
Vue.use(ElementUI, {locale,size: 'medium'})// set element-ui default size

// 全局组件挂载
Vue.component('Pagination', Pagination)

// 全局方法挂载
Vue.prototype.getDicts = getDicts
Vue.prototype.getConfigKey = getConfigKey
Vue.prototype.parseTime = parseTime
Vue.prototype.resetForm = resetForm
Vue.prototype.addDateRange = addDateRange
Vue.prototype.selectDictLabel = selectDictLabel
Vue.prototype.download = download
Vue.prototype.handleTree = handleTree
Vue.config.productionTip = false
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
